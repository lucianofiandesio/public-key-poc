package com.euroclear;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import static com.euroclear.KeyGen.RSA;

/**
 * Hello world!
 */
public class App {
    private final static String PLAIN_TEXT = "I'm plain text!";

    public static void main(String[] args) throws Exception {

        // Encrypt/Decrypt using asymmetric encryption


        // Step 1 - generate key pair
        KeyPair kp = KeyGen.generateRSAKkeyPair();
        Hex hex = new Hex();
        RSAPublicKey pub = (RSAPublicKey) kp.getPublic();//
        RSAPrivateKey prv = (RSAPrivateKey) kp.getPrivate();//

        // The public key should be distributed with the clients as a normal config file
        final String pubKey = new String(Base64.encodeBase64(pub.getEncoded()));
        // The private key should be stored somewhere, but never distributed
        final String prvKey = new String(Base64.encodeBase64(prv.getEncoded()));

        System.out.println("private: " + prvKey);
        System.out.println("public: " + pubKey);

        // Step 2 - encrypt
        byte[] encrypted = Crypto.RSAEncrypt(PLAIN_TEXT, prv);
        System.out.println("encrypt: " + Base64.encodeBase64String(encrypted));

        // Step 3 - decrypt
        String plain = Crypto.RSADecrypt(encrypted, getPublicKeyFromString(pubKey));

        System.out.println("plain text: " + plain);
        assert plain.equals(PLAIN_TEXT);

        // ---------------------------------------------------------------------

        // Encrypt using symmetric encryption

        SecretKey key = Crypto.getKeyFromPassword("my-super-secret-password", "some-random-string");
        IvParameterSpec ivParameterSpec = Crypto.generateIv();

        String algorithm = "AES/CBC/PKCS5Padding";
        byte[] cipherText = Crypto.encrypt(algorithm, PLAIN_TEXT, key, ivParameterSpec);
        String plainText = Crypto.decrypt(algorithm, cipherText, key, ivParameterSpec);

        assert plainText.equals(PLAIN_TEXT);



    }

    private static RSAPublicKey getPublicKeyFromString(String pk) throws NoSuchAlgorithmException, InvalidKeySpecException {

        KeyFactory kf = KeyFactory.getInstance(RSA);

        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.decodeBase64(pk));
        return (RSAPublicKey) kf.generatePublic(keySpecX509);

    }
}
